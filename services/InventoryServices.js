import React from 'react';
import { API_URL } from './api';

export const updateProductQuantityInInventory = async (accessToken) => {
    const data = {
        prodId: 1,
        invenId: 1,
        quantity: 0
    }

    const headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Authorization": 'Bearer ' + accessToken
    }

    isProductExistInInventory(data, headers);
}

const isProductExistInInventory = (data, headers) => {
    return new Promise((resolve, reject) => {
        fetch(API_URL + `InventoryDetails/InventoryDetailsByBill?ProductId=${encodeURIComponent(data.prodId)}&InventoryId=${encodeURIComponent(data.invenId)}`, {
            method: 'GET',
            headers: headers
        })
            .then(res => res.json())
            .then(resJson => console.log(resJson))
            .catch(err => reject(err))
    })
}