import React, { useEffect } from 'react';
import { View, Text, Image, StyleSheet, ActivityIndicator } from 'react-native';
import { isSignedIn } from '../auth';
import Colors from '../constants/colors';

const SplashScreen = props => {
    useEffect(() => {
        isSignedIn()
            .then(res => props.navigation.navigate(res ? "SignIn" : "SignOut"))
            .catch(() => alert("An error occurred!"));
    }, []);

    return (
        <View style={styles.container}>
            <Image source={require('../assets/logo.png')} style={styles.logo} />
            <Text style={styles.title}>Shop Ware</Text>
            <ActivityIndicator size="large" color={Colors.primary} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.splashBg,
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    logo: {
        width: 300,
        height: 300,
        resizeMode: 'cover',
        marginBottom: 20
    },
    title: {
        fontSize: 30,
        fontWeight: "600",
        color: Colors.primary,
        textTransform: "uppercase",
        marginBottom: 30
    }
});

export default SplashScreen;