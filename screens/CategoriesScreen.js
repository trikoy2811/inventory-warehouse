import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Header from '../components/Header';
import BillDetail from '../components/BillDetail';

const CategoriesScreen = () => {
    return (
        <View style={styles.container}>
            <Header titleText="Import" />
            <BillDetail />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#ccc"
    }
});

export default CategoriesScreen;