import React, { useState } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import Header from '../components/Header';
import InventoriesList from '../components/InventoriesList';
import { getToken } from '../auth';



const InventoryScreen = props => {
    return (
        <View style={styles.container}>
            <Header titleText="Inventory" />
            <InventoriesList navigation={props.navigation} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#ccc",
    }
});

export default InventoryScreen;