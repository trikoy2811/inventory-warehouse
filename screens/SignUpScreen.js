import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import Colors from '../constants/colors';

const SignUpScreen = props => {
    return (
        <View style={styles.container}>
            <Image source={require('../assets/logo.png')} style={styles.logo} />
            <Text>SignUpScreen</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.splashBg,
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    logo: {
        width: 300,
        height: 300,
        resizeMode: 'cover',
        marginBottom: 20
    },
    title: {
        fontSize: 30,
        fontWeight: "600",
        color: Colors.primary,
        textTransform: "uppercase",
        marginBottom: 30
    }
});

export default SignUpScreen;