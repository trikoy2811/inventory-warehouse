import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Header from '../components/Header';
import BillDetail from '../components/BillDetail';
import AddProductInBill from '../components/AddProductInBill';
import { getUser } from '../auth';
import { API_URL } from '../services/api';
import moment from 'moment';


const BillDetailScreen = props => {
    const [defaultProductList, setDefaultProductList] = useState([]);
    const [isAddMode, setIsAddMode] = useState(false);
    const [token, setToken] = useState("");
    const [refresh, setRefresh] = useState(false);

    useEffect(() => {
        const abcxyz = () => {
            getUser()
                .then(res => {
                    const value = JSON.parse(res);
                    getProduct(value.access_token);
                    setToken(value.access_token);
                })
                .catch(err => console.log(err))
        };
        abcxyz();
    }, []);

    const headers = {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token
    };

    const getProduct = async (accessToken) => {
        const id = JSON.stringify(props.navigation.getParam('inventoryId', -1));
        const type = parseInt(JSON.stringify(props.navigation.getParam('type', -1)));
        const query = (type === 0) ? 'Products' : `Product/ProductsByInventory?id=${id}`;
        await fetch(API_URL + query, {
            method: 'GET',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Authorization": 'Bearer ' + accessToken
            }
        })
            .then((res) => res.json())
            .then((responseJson) => {
                setDefaultProductList(responseJson);
            })
            .catch((err) => {
                console.log(err)
            })
    };

    const addProductHandler = (product, quantity) => {
        const data = {
            productId: product,
            inventoryId: JSON.stringify(props.navigation.getParam('inventoryId', -1)),
            quantity: quantity,
            type: parseInt(JSON.stringify(props.navigation.getParam('type', -1))),
            billId: JSON.stringify(props.navigation.getParam('billId', -1))
        }

        addProductToBill(data.billId, data.productId, data.quantity);
        getInventoryDetailByProductAndInventory(data.productId, data.inventoryId, data.quantity, data.type);
    }

    const addProductToBill = async (billID, product, quantity) => {
        await fetch(API_URL + 'BillDetails', {
            method: 'POST',
            headers: headers,
            body: JSON.stringify({
                BillId: billID,
                ProductId: product,
                Price: 0.0,
                QuantityRequest: quantity,
                QuantityActual: quantity,
                isActive: true,
                note: null
            })
        })
            .then(res => res.json())
            .then(resJson => {
                setIsAddMode(false);
                setRefresh(true);
            })
            .catch(err => console.log(err))
    }

    const getInventoryDetailByProductAndInventory = async (productId, inventoryId, quantity, type) => {
        await fetch(API_URL + `InventoryDetails/InventoryDetailsByBill?ProductId=${encodeURIComponent(productId)}&InventoryId=${encodeURIComponent(inventoryId)}`, {
            method: 'GET',
            headers: headers
        })
            .then(res => res.json())
            .then(resJson => {
                if (resJson.length > 0) {
                    const oldStock = resJson[0].Quantity;
                    const invenDetailId = resJson[0].InventoryDetailsId;

                    const newStock = (type === 0) ? (parseInt(oldStock) + parseInt(quantity)) : (parseInt(oldStock) - parseInt(quantity));
                    updateQuantity(invenDetailId, productId, inventoryId, newStock);
                } else {
                    createNewQuantity(productId, inventoryId, quantity);
                }
            })
            .catch(err => console.log(err))
    }

    const createNewQuantity = async (productId, inventoryId, quantity) => {
        await fetch(API_URL + 'InventoryDetails', {
            method: 'POST',
            headers: headers,
            body: JSON.stringify({
                InventoryId: inventoryId,
                ProductId: productId,
                Price: 1.1,
                Quantity: quantity,
                expiryDate: moment().format(),
                isActive: true
            })
        })
            .then(res => res.json())
            .catch(err => console.log(err))
    }

    const updateQuantity = async (inventoryDetailId, productId, inventoryId, quantity) => {
        var active = true;
        if (quantity === 0) {
            active = 0;
        }
        await fetch(API_URL + `InventoryDetails?id=${inventoryDetailId}`, {
            method: 'PUT',
            headers: headers,
            body: JSON.stringify({
                InventoryDetailId: inventoryDetailId,
                InventoryId: inventoryId,
                ProductId: productId,
                Quantity: quantity,
                isActive: active
            })
        })
            .then(res => res.json())
            .then(resJson => console.log(resJson))
            .catch(err => console.log(err));
    }

    return (
        <View style={styles.container}>
            <Header titleText={JSON.stringify(props.navigation.getParam('billTitle', 'Bill Detail'))} />
            <View>
                <Text></Text>
            </View>
            <BillDetail
                navigation={props.navigation}
                refresh={refresh}
                doneRefresh={() => setRefresh(false)}
                billID={JSON.stringify(props.navigation.getParam('billId', -1))} />
            <View style={styles.bottomButtonContainer} >
                <View style={styles.buttonFlexBox}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Bills')}>
                        <View style={styles.bottomButton}>
                            <Text style={styles.buttonText}>Back</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setIsAddMode(true)}>
                        <View style={styles.bottomButton}>
                            <Text style={styles.buttonText}>Add Product</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            <AddProductInBill
                onAddProduct={addProductHandler}
                onCancel={() => setIsAddMode(false)}
                products={defaultProductList}
                visible={isAddMode}
                type={parseInt(JSON.stringify(props.navigation.getParam('type', -1)))}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#ccc"
    },
    bottomButtonContainer: {
        backgroundColor: "#fff",
        padding: 20,
        marginVertical: 10,
        width: '100%',
        height: 90,
    },
    buttonFlexBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%'
    },
    bottomButton: {
        backgroundColor: '#D4AC0D',
        width: 150,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        textTransform: 'uppercase',
        fontSize: 18,
        fontWeight: '500',
        alignSelf: 'center',
        color: '#fff'
    }
});

export default BillDetailScreen;