import React from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { onSignOut } from '../auth';
import Contact from '../components/Contact';
import Header from '../components/Header';
const ProfileScreen = props => {
    return (
        <View style={styles.container}>
            <Header titleText="Profile" />
            <Contact />
            <View style={styles.bottomButtonContainer} >
                <Button
                    title="Log out"
                    color="red"
                    onPress={() => { onSignOut().then(() => props.navigation.navigate('SignOut')) }}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#ccc"
    },
    bottomButtonContainer: {
        backgroundColor: "#fff",
        padding: 20,
        marginVertical: 20,
        width: '100%'
    }
});

export default ProfileScreen;