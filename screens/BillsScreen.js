import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Button, TouchableOpacity, ActivityIndicator } from 'react-native';
import Header from '../components/Header';
import BillsList from '../components/BillsList';
import { API_URL } from '../services/api';
import { getUser } from '../auth';
import ImportForm from '../components/ImportForm';
import moment from 'moment';
import { updateProductQuantityInInventory } from '../services/InventoryServices';

const BillsScreen = props => {
    const [billsList, setBillsList] = useState([]);
    const [inventoriesList, setInventoriesList] = useState([]);
    const [employee, setEmployee] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    const [isAddMode, setIsAddMode] = useState(false);
    const [token, setToken] = useState("");
    const [ready, setReady] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        // setIsAddMode(false);
        const abcxyz = () => {
            getUser()
                .then(res => {
                    const value = JSON.parse(res);
                    setToken(value.access_token);
                    setEmployee(value.userName);
                    getBill(value.access_token);
                    getInventory(value.access_token);
                })
                .catch(err => console.log(err))
        }
        abcxyz();
    }, []);

    const getBill = async (accessToken) => {
        const auth = "Bearer " + accessToken;
        const headers = {
            Accept: "application/json",
            "Content-Type": "application/json",
            "Authorization": auth
        }
        await fetch(API_URL + 'Bills', {
            method: 'GET',
            headers: headers
        })
            .then((res) => res.json())
            .then((responseJson) => {
                setBillsList(responseJson);
                setIsLoading(false);
            })
            .catch((err) => {
                console.log(err)
            })
    };

    const getInventory = async (accessToken) => {
        const auth = "Bearer " + accessToken;
        const headers = {
            Accept: "application/json",
            "Content-Type": "application/json",
            "Authorization": auth
        }
        await fetch(API_URL + 'Inventories', {
            method: 'GET',
            headers: headers
        })
            .then((res) => res.json())
            .then((responseJson) => {
                setInventoriesList(responseJson);
                setReady(true);
            })
            .catch((err) => {
                console.log(err)
            })
    }

    const addBillHandler = async (inventory, type) => {
        const headers = {
            Accept: "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token
        };
        const today = moment().format();
        await fetch(API_URL + 'Bills', {
            method: 'POST',
            headers: headers,
            body: JSON.stringify({
                InventoryId: inventory,
                Type: type,
                EmployeeId: 1,
                DateCreated: today,
                DateModified: today,
                TotalAmount: 0.0,
                isActive: true
            })
        })
            .then(res => res.json())
            .then(resJ => {
                setIsAddMode(false);
                props.navigation.navigate('BillDetail', {
                    billId: resJ.BillId,
                    inventoryId: resJ.InventoryId,
                    billTitle: getInventoryName(resJ.InventoryId),
                    type: resJ.Type
                });
            })
            .catch(err => console.log(err))
        // setIsAddMode(false);
    }

    const getInventoryName = (id) => {
        const current = inventoriesList.filter(e => e.InventoryId === id);
        return current[0].Name;
    }

    let inputButton = <View><Text>Loading...</Text></View>;

    if (ready) {
        inputButton = (
            <View style={styles.bottomButtonContainer} >
                <View style={styles.buttonFlexBox}>
                    <TouchableOpacity onPress={() => setIsAddMode(true)}>
                        <View style={styles.bottomButton}>
                            <Text style={styles.buttonText}>Add new</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    if (isLoading) {
        return (
            <View style={styles.loader}>
                <ActivityIndicator size='large' />
            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <Header titleText="Bills" />
                <BillsList listItem={billsList} navigation={props.navigation} />
                {inputButton}
                <ImportForm
                    onAddBill={addBillHandler}
                    onCancel={() => setIsAddMode(false)}
                    inventories={inventoriesList}
                    visible={isAddMode}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    loader: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#ccc"
    },
    bottomButtonContainer: {
        backgroundColor: "#fff",
        padding: 20,
        marginVertical: 10,
        width: '100%',
        height: 90,
    },
    buttonFlexBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%'
    },
    bottomButton: {
        backgroundColor: '#D4AC0D',
        width: 150,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        textTransform: 'uppercase',
        fontSize: 18,
        fontWeight: '500',
        alignSelf: 'center',
        color: '#fff'
    }
});

export default BillsScreen;