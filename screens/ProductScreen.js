import React, { useState } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import Header from '../components/Header';
import ProductsList from '../components/ProductsList';



const ProductScreen = props => {
    return (
        <View style={styles.container}>
            <Header titleText="Product" />
            <ProductsList navigation={props.navigation} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#ccc",
    }
});

export default ProductScreen;