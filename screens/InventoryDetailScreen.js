import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Header from '../components/Header';
import InventoryDetail from '../components/InventoryDetail';

const InventoryDetailScreen = props => {
    return (
        <View style={styles.container}>
            <Header titleText={JSON.stringify(props.navigation.getParam('titleText','Inventory Detail'))} />
            <InventoryDetail navigation={props.navigation} inventoryID={JSON.stringify(props.navigation.getParam('inventoryId',-1))}/>
            <View style={styles.bottomButtonContainer} >
                <View style={styles.buttonFlexBox}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Inventories')}>
                        <View style={styles.bottomButton}>
                            <Text style={styles.buttonText}>Back</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#ccc"
    },
    bottomButtonContainer: {
        backgroundColor: "#fff",
        padding: 20,
        marginVertical: 10,
        width: '100%',
        height: 90,
    },
    buttonFlexBox: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%'
    },
    bottomButton: {
        backgroundColor: '#D4AC0D',
        width: 150,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        textTransform: 'uppercase',
        fontSize: 18,
        fontWeight: '500',
        alignSelf: 'center',
        color: '#fff'
    }
});

export default InventoryDetailScreen;