import React from 'react';
import { Text, View, StyleSheet, Image, KeyboardAvoidingView } from 'react-native';
import LoginForm from '../components/LoginForm';
import Colors from '../constants/colors';

const LoginScreen = props => {
    return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <Image source={require('../assets/logo.png')} style={styles.logo} />
                <LoginForm navigation={props.navigation} />
            </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.splashBg,
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    logo: {
        width: 250,
        height: 250,
        resizeMode: 'cover',
        marginBottom: 20
    },
    title: {
        fontSize: 30,
        fontWeight: "600",
        color: Colors.primary,
        textTransform: "uppercase",
        marginBottom: 30
    }
});

export default LoginScreen;