import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import moment from 'moment';

const BillItem = props => {

const checkType = () => {
    if(props.bill.Type > 2 || props.bill.Type < 0 || props.bill.Type === null) {
        return "Error"
    }
    return (props.bill.Type === 0) ? 'Import' : 'Export'
}

    return (
        <TouchableOpacity
            onPress={() => {
                props.navigation.navigate('BillDetail', {
                    billId: props.bill.BillId,
                    billTitle: props.bill.Inventory.Name,
                    inventoryId: props.bill.Inventory.InventoryId,
                    type: props.bill.Type
                })
            }}
        >
            <View style={styles.container}>
                <View style={styles.infoArea}>
                    <Text style={styles.infoTitle}>{props.bill.Inventory.Name}</Text>
                    <Text style={styles.infoAdd}>Date: {moment(props.bill.DateCreated).format("MMM Do YYYY")}</Text>
                    <Text style={styles.infoAdd}>Type: {checkType()}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 100,
        marginHorizontal: 10,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        marginBottom: 5
    },
    infoArea: {
        width: '100%',
        paddingLeft: 5,
        paddingVertical: 5,
        height: 70,
    },
    infoTitle: {
        fontSize: 18,
        fontWeight: '500',
        marginBottom: 5
    },
    infoAdd: {
        color: "#353b48",
        fontSize: 12,
        marginBottom: 2
    },
    quantityArea: {
        flexDirection: 'row-reverse',
        flex: 1
    },
    quantityItem: {
        width: 100,
        color: 'green',
        fontSize: 15
    }

});

export default BillItem;