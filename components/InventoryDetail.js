import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import InventoryDetailItem from './InventoryDetailItem';
import { getUser } from '../auth';
import { API_URL } from '../services/api'

const InventoryDetail = props => {
    const [productList, setProductList] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const abcxyz = () => {
            getUser()
                .then(res => {
                    const value = JSON.parse(res);
                    getProductsByInventoryId(value.access_token);
                })
                .catch(err => console.log(err))
        };
        abcxyz();
    }, []);

    const getProductsByInventoryId = async (accessToken) => {
        const data = {
            id: props.inventoryID
        }
        await fetch(API_URL + `Product/ProductsByInventory?id=${encodeURIComponent(data.id)}`, {
            method: 'GET',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Authorization": 'Bearer ' + accessToken
            }
        })
            .then(res => res.json())
            .then(resJson => {
                setProductList(resJson);
                setIsLoading(false);
            })
            .catch(err => console.log(err))
    }

    if (isLoading) {
        return (
            <View style={styles.errorContainer}>
                <ActivityIndicator size='large' />
            </View>
        )
    } else if(productList.length > 0 ){
        return (
            <View style={styles.container}>
                <FlatList
                    data={productList}
                    renderItem={itemData => {
                        return <InventoryDetailItem inventoryDetail={itemData.item} />
                    }}
                    keyExtractor={
                        (item, index) => index.toString()
                    }
                />
            </View>
        );
    } else {
        return (
            <View style={styles.errorContainer}>
                <Text style={styles.errorText}>No product is here</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        width: '100%',
        flex: 1,
        padding: 10,
        marginBottom: 10
    },
    errorContainer: {
        backgroundColor: 'white',
        flex: 1,
        width: '100%',
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    errorText: {
        color: 'red',
        fontSize: 17,
        fontWeight: '500'
    }
});

export default InventoryDetail;