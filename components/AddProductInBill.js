import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Modal, Picker, TextInput, TouchableOpacity } from 'react-native';

const AddProductInBill = props => {
    const [productId, setProductId] = useState(-1);
    const [maximum, setMaximum] = useState(0);
    const [quantity, setQuantity] = useState("");
    const [mes, setMes] = useState("");

    useEffect(() => {
        filter(productId);
    }, [productId]);

    const filter = (id) => {
        if (props.type === 0) {
            return;
        }
        if (id < 0) {
            setMaximum(0);
            return;
        }
        const list = props.products;
        const current = list.filter(e => e.Product.ProductId == id);
        setMaximum(current[0].Quantity);
    }

    const inputQuantityHandler = (enterText) => {
        if (!isNaN(enterText))
            setQuantity(enterText);
    }

    const iPickerItemRender = () => {
        let item = [];
        if (props.type === 0) {
            props.products.map((obj, index) => {
                item.push(<Picker.Item key={index} label={obj.Name} value={obj.ProductId} />)
            });
        } else {
            props.products.map((obj, index) => {
                item.push(<Picker.Item key={index} label={obj.Product.Name} value={obj.Product.ProductId} />)
            });
        }
        return item;
    }

    const submitProductHandler = () => {
        if (productId < 0) {
            setMes("Please choose Product!!");
            return;
        }
        const value = quantity;
        if (!value) {
            setMes("Quantity cannot be blank!!");
            return;
        }
        if (value <= 0) {
            setMes("Quantity must be greater than 0");
            return;
        }
        if (props.type === 1) {
            if (value > maximum) {
                setMes("Don't have enough for you!!");
                return;
            }
        }

        props.onAddProduct(productId, quantity);
        setProductId(-1);
        setQuantity("");
    }

    let max = <View><Text></Text></View>

    if (props.type === 1) {
        max = (
            <View style={styles.errorContainer}>
                <Text style={styles.errorText}>In-stock: {maximum}</Text>
            </View>
        )
    }

    return (
        <Modal visible={props.visible} animationType="slide">
            <View style={styles.inputContainer}>
                <View style={styles.errorContainer}>
                    <Text style={styles.errorText}>{mes}</Text>
                </View>
                {max}
                <View style={styles.formGroup}>
                    <Text style={styles.formItem}>Product:</Text>
                    <View style={styles.pickerFormContainer}>
                        <Picker
                            style={styles.pickerForm}
                            selectedValue={productId}
                            onValueChange={(itemValue, itemIndex) => setProductId(itemValue)}
                        >
                            <Picker.Item key={999999} label="<Choose Product>" value={-1} />
                            {iPickerItemRender()}
                        </Picker>
                    </View>
                </View>
                <View style={styles.formGroup}>
                    <Text style={styles.formItem}>Quantity:</Text>
                    <TextInput
                        style={styles.input}
                        value={quantity}
                        onChangeText={inputQuantityHandler}
                        keyboardType="numeric"
                    />
                </View>
                <View style={styles.formGroup}>
                    <TouchableOpacity onPress={props.onCancel}>
                        <View style={styles.cancelBottomButton}>
                            <Text style={styles.buttonText}>Cancel</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={submitProductHandler}>
                        <View style={styles.submitBottomButton}>
                            <Text style={styles.buttonText}>OK</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    inputContainer: {
        width: '100%',
        flex: 1,
        backgroundColor: '#fff',
        marginBottom: 10,
        justifyContent: 'center'
    },
    formGroup: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-between',
        marginBottom: 10
    },
    input: {
        height: 50,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 15,
        width: 300,
        paddingLeft: 10
    },
    formItem: {
        lineHeight: 50,
        marginRight: 10
    },
    pickerForm: {
        height: 50,
        width: 300,
    },
    pickerFormContainer: {
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 15,
    },
    cancelBottomButton: {
        backgroundColor: '#ccc',
        width: 150,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15
    },
    submitBottomButton: {
        backgroundColor: '#D4AC0D',
        width: 150,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15
    },
    buttonText: {
        textTransform: 'uppercase',
        fontSize: 18,
        fontWeight: '500',
        alignSelf: 'center',
        color: '#fff'
    },
    errorContainer: {
        marginBottom: 5,
        alignItems: 'center'
    },
    errorText: {
        color: 'red',
        fontWeight: '500'
    }
});

export default AddProductInBill;