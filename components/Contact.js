import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { getUser } from '../auth';

const Contact = () => {
    const [userInfo, setUserInfo] = useState('');
    useEffect(() => {
        const getInfo = () => {
            getUser()
                .then(res => {
                    const value = JSON.parse(res);
                    setUserInfo(value.userName)
                })
                .catch(err => console.log(err))
        }
        getInfo();
    }, []);

    return (
        <View style={styles.container}>
            <Image source={require('../assets/contact.png')} style={styles.contactImg} />
            <View style={styles.infoContainer}>
                <Text style={styles.infoItem}>Name: {userInfo}</Text>
                <Text style={styles.infoItem}>Email: {userInfo}</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        width: '100%',
        padding: 10,
        alignItems: 'center'
    },
    contactImg: {
        marginTop: 30,
        width: 300,
        height: 300,
        resizeMode: 'cover',
        marginBottom: 30
    },
    infoContainer: {
        width: '100%',
        padding: 10
    },
    infoItem: {
        fontSize: 18,
        fontWeight: '500',
        marginBottom: 10
    }
});

export default Contact;