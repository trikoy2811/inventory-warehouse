import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import ProductDetailItem from './ProductDetailItem';
import { getUser } from '../auth';
import { API_URL } from '../services/api'

const ProductDetail = props => {
    const [inventoryList, setInventoryList] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const abcxyz = () => {
            getUser()
                .then(res => {
                    const value = JSON.parse(res);
                    getInventoryByProductId(value.access_token);
                })
                .catch(err => console.log(err))
        };
        abcxyz();
    }, []);

    const getInventoryByProductId = async (accessToken) => {
        const data = {
            id: props.productID
        }
        await fetch(API_URL + `InventoryDetails/InventoryDetailsByProduct?id=${encodeURIComponent(data.id)}`, {
            method: 'GET',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Authorization": 'Bearer ' + accessToken
            }
        })
            .then(res => res.json())
            .then(resJson => {
                setInventoryList(resJson);
                setIsLoading(false);
            })
            .catch(err => console.log(err))
    }

    if (isLoading) {
        return (
            <View style={styles.errorContainer}>
                <ActivityIndicator size='large' />
            </View>
        )
    } else if (inventoryList.length > 0) {
        return (
            <View style={styles.container}>
                <FlatList
                    data={inventoryList}
                    renderItem={itemData => {
                        return <ProductDetailItem productDetail={itemData.item} />
                    }}
                    keyExtractor={
                        (item) => item.InventoryDetailsId.toString()
                    }
                />
            </View>
        );
    } else {
        return (
            <View style={styles.errorContainer}>
                <Text style={styles.errorText}>Out of stock</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        width: '100%',
        flex: 1,
        padding: 10,
        marginBottom: 10
    },
    errorContainer: {
        backgroundColor: 'white',
        flex: 1,
        width: '100%',
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    errorText: {
        color: 'red',
        fontSize: 17,
        fontWeight: '500'
    }
});

export default ProductDetail;