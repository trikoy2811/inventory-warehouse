import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import BillItem from './BillItem';


const BillsList = props => {

    return (
        <View style={styles.container}>
            <FlatList
                data={props.listItem.reverse()}
                renderItem={itemData => {
                    return <BillItem bill={itemData.item} navigation={props.navigation} />
                }}
                keyExtractor={
                    (item) => item.BillId.toString()
                }
            />
        </View>
    );
}



const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        width: '100%',
        flex: 1,
        padding: 10,
    },
});

export default BillsList;