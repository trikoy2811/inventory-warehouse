import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { onSignIn } from '../auth';
import Colors from '../constants/colors';

const LoginForm = props => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [mes, setMes] = useState("");

    const usernameInputHandler = (inputText) => {
        setUsername(inputText);
    }

    const passwordInputHandler = (inputText) => {
        setPassword(inputText);
    }

    const checkLogin = async () => {
        await fetch('https://inventorymanagement-api.azurewebsites.net/token', {
            method: 'POST',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: "grant_type=password&username=" + username + "&password=" + password
        })
            .then(res => {
                if (res.status < 400) {
                    return res.json();
                }
                return null;
            })
            .then(resJson => {
                if (resJson) {
                    onSignIn(resJson);
                    props.navigation.navigate('SignIn');
                } else {
                    setMes("Username or password is incorrect!!");
                }
            })
            .catch(err => console.log(err));
        // return;
    }
    return (
        <View style={styles.formContainer}>
            <View style={styles.errorContainer}>
                <Text style={styles.errorText}>{mes}</Text>
            </View>
            <TextInput
                placeholder="Username or Email"
                placeholderTextColor="rgba(162, 155, 254, 0.7)"
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="email-address"
                onChangeText={usernameInputHandler}
                value={username}
                style={styles.input}
            />
            <TextInput
                placeholder="Password"
                placeholderTextColor="rgba(162, 155, 254, 0.7)"
                autoCapitalize="none"
                autoCorrect={false}
                secureTextEntry
                onChangeText={passwordInputHandler}
                value={password}
                style={styles.input}
            />
            <TouchableOpacity style={styles.button} onPress={checkLogin}>
                <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    formContainer: {
        marginBottom: 80,
        width: '90%',
        alignSelf: 'center'
    },
    input: {
        height: 50,
        backgroundColor: 'white',
        opacity: 0.8,
        marginBottom: 15,
        padding: 15,
        borderRadius: 15,
        textAlign: 'center'
    },
    button: {
        backgroundColor: Colors.primary,
        alignItems: 'center',
        justifyContent: 'center',
        height: 50,
        borderRadius: 15
    },
    buttonText: {
        fontSize: 18,
        fontWeight: '600',
        color: 'white'
    },
    errorContainer: {
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    errorText: {
        fontWeight: '500',
        color: 'red'
    }
})

export default LoginForm;