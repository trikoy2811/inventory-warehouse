import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

const Header = props => {
    return (
        <View style={styles.container}>
            <Text style={styles.headingText}>{props.titleText}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        paddingTop: 35,
        paddingBottom: 15,
        width: '100%',
        alignItems: 'center',
        marginBottom: 20
    },
    headingText: {
        fontSize: 18,
        fontWeight: '600',
        textTransform: 'uppercase'
    }
});

export default Header;