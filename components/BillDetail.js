import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import BillDetailItem from './BillDetailItem';
import { getUser } from '../auth';
import { API_URL } from '../services/api';

const BillDetail = props => {
    const [productList, setProductList] = useState([]);
    const [token, setToken] = useState("");
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const abcxyz = () => {
            getUser()
                .then(res => {
                    const value = JSON.parse(res);
                    setToken(value.access_token);
                    getProductsByBillId(value.access_token);
                })
                .catch(err => console.log(err))
        };
        abcxyz();
    }, []);

    const getProductsByBillId = async (accessToken) => {
        const data = {
            id: props.billID
        }
        await fetch(API_URL + `Product/ProductsByBill?id=${encodeURIComponent(data.id)}`, {
            method: 'GET',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Authorization": 'Bearer ' + accessToken
            }
        })
            .then(res => res.json())
            .then(resJson => {
                setProductList(resJson);
                setIsLoading(false);
                props.doneRefresh();
            })
            .catch(err => console.log(err))
    }

    useEffect(()=>{
        if(props.refresh){
            getProductsByBillId(token);
        }
    },[props.refresh])

    if (isLoading) {
        return (
            <View style={styles.errorContainer}>
                <ActivityIndicator size='large' />
            </View>
        )
    } else if (productList.length > 0) {
        return (
            <View style={styles.container}>
                <FlatList
                    data={productList.reverse()}
                    renderItem={itemData => {
                        return <BillDetailItem billDetail={itemData.item} />
                    }}
                    keyExtractor={
                        (item) => item.BillDetailId.toString()
                    }
                />
            </View>
        );
    } else {
        return (
            <View style={styles.errorContainer}>
                <Text style={styles.errorText}>No product is here</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        width: '100%',
        flex: 1,
        padding: 10,
        marginBottom: 10
    },
    errorContainer: {
        backgroundColor: 'white',
        flex: 1,
        width: '100%',
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    errorText: {
        color: 'red',
        fontSize: 17,
        fontWeight: '500'
    }
});

export default BillDetail;