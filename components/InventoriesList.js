import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ActivityIndicator, FlatList } from 'react-native';
import InventoryItem from './InventoryItem';
import { API_URL } from '../services/api';
import { getUser } from '../auth';

const InventoriesList = props => {

    const [productList, setProductList] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        const abcxyz = () => {
            getUser()
                .then(res => {
                    const value = JSON.parse(res);
                    getInventory(value.access_token);
                })
                .catch(err => console.log(err))
        };
        abcxyz();
    }, []);

    const getInventory = async (accessToken) => {
        await fetch(API_URL + 'Inventories', {
            method: 'GET',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Authorization": 'Bearer ' + accessToken
            }
        })
            .then((res) => res.json())
            .then((responseJson) => {
                setProductList(responseJson);
                // console.log(responseJson);
                setIsLoading(false);
            })
            .catch((err) => {
                console.log(err)
            })
    };

    if (isLoading) {
        return (
            <View style={styles.loader}>
                <ActivityIndicator size='large' />
            </View>
        )
    }
    else {
        return (
            <View style={styles.container}>
                <FlatList
                    data={productList}
                    renderItem={itemData => {
                        return <InventoryItem inventory={itemData.item} navigation={props.navigation}/>
                    }}
                    keyExtractor={(item) => item.InventoryId.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        flex: 1,
        width: '100%',
        padding: 10,
        alignContent: 'flex-start',
        justifyContent: 'space-around',
        marginBottom: 20,
    },
    loader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        width: '100%'
    }
});

export default InventoriesList;