import React from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';

const InventoryItem = props => {
    return (
        <TouchableWithoutFeedback onPress={() => { props.navigation.navigate('InventoryDetail',{
            inventoryId: props.inventory.InventoryId,
            titleText: props.inventory.Name
        }) }}>
            <View style={styles.container}>
                <View style={styles.infoArea}>
                    <Text style={styles.infoTitle}>{props.inventory.Name}</Text>
                    <Text style={styles.infoAddr}>{props.inventory.Describe}</Text>
                </View>
                {/* <View style={styles.quantityArea}>
                    <Text style={styles.quantityItem}>Total: 4</Text>
                </View> */}
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 100,
        marginHorizontal: 10,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        marginBottom: 5
    },
    infoArea: {
        width: '100%',
        paddingLeft: 5,
        paddingVertical: 5,
        height: 70,
    },
    infoTitle: {
        fontSize: 18,
        fontWeight: '500',
    },
    infoAddr: {
        color: "#353b48",
        fontSize: 12,
        marginTop: 5
    },
    quantityArea: {
        flexDirection: 'row-reverse',
        flex: 1
    },
    quantityItem: {
        width: 100,
        color: 'green',
        fontSize: 15
    }

});

export default InventoryItem;