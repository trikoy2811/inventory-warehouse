import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, TextInput, Picker, TouchableOpacity, Modal } from 'react-native';
const ImportForm = props => {
    const [type, setType] = useState(-1);
    const [inventoryId, setInventoryId] = useState(-1);
    const [mes, setMes] = useState("");

    const submitBillHandler = () => {
        if (inventoryId < 0) {
            setMes("Please choose inventory!!");
            return;
        }
        if (type < 0) {
            setMes("Please choose bill type!!");
            return;
        }
        props.onAddBill(inventoryId, type);
        setType(-1);
        setInventoryId(-1);
    }

    const iPickerItemRender = () => {
        let item = [];
        props.inventories.map(obj => {
            item.push(<Picker.Item key={obj.InventoryId} label={obj.Name} value={obj.InventoryId} />)
        });
        return item;
    }

    return (
        <Modal visible={props.visible} animationType="slide">
            <View style={styles.inputContainer}>
                <View style={styles.errorContainer}>
                    <Text style={styles.errorText}>{mes}</Text>
                </View>
                <View style={styles.formGroup}>
                    <Text style={styles.formItem}>Inventory:</Text>
                    <View style={styles.pickerFormContainer}>
                        <Picker
                            style={styles.pickerForm}
                            selectedValue={inventoryId}
                            onValueChange={(itemValue, itemIndex) => setInventoryId(itemValue)}
                        >
                            <Picker.Item key={999999} label="<Choose Inventory>" value={-1} />
                            {iPickerItemRender()}
                        </Picker>
                    </View>
                </View>
                <View style={styles.formGroup}>
                    <Text style={styles.formItem}>Type</Text>
                    <View style={styles.pickerFormContainer}>
                        <Picker
                            style={styles.pickerForm}
                            selectedValue={type}
                            onValueChange={(itemValue, itemIndex) => setType(itemValue)}
                        >
                            <Picker.Item label="<Choose type>" value="-1" />
                            <Picker.Item label="Import" value="0" />
                            <Picker.Item label="Export" value="1" />
                        </Picker>
                    </View>
                </View>
                <View style={styles.formGroup}>
                    <Text style={styles.formItem}>Note:</Text>
                    <TextInput
                        style={styles.input}
                    />
                </View>
                <View style={styles.formGroup}>
                    <TouchableOpacity onPress={props.onCancel}>
                        <View style={styles.cancelBottomButton}>
                            <Text style={styles.buttonText}>Cancel</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={submitBillHandler}>
                        <View style={styles.submitBottomButton}>
                            <Text style={styles.buttonText}>OK</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
}

const styles = StyleSheet.create({
    inputContainer: {
        width: '100%',
        flex: 1,
        backgroundColor: '#fff',
        marginBottom: 10,
        justifyContent: 'center'
    },
    formGroup: {
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-between',
        marginBottom: 10
    },
    input: {
        height: 50,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 15,
        width: 300,
        paddingLeft: 10
    },
    formItem: {
        lineHeight: 50,
        marginRight: 10
    },
    pickerForm: {
        height: 50,
        width: 300,
    },
    pickerFormContainer: {
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 15,
    },
    cancelBottomButton: {
        backgroundColor: '#ccc',
        width: 150,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15
    },
    submitBottomButton: {
        backgroundColor: '#D4AC0D',
        width: 150,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15
    },
    buttonText: {
        textTransform: 'uppercase',
        fontSize: 18,
        fontWeight: '500',
        alignSelf: 'center',
        color: '#fff'
    }, 
    errorContainer: {
        marginBottom: 5,
        alignItems: 'center'
    },
    errorText: {
        color: 'red',
        fontWeight: '500'
    }
});

export default ImportForm;