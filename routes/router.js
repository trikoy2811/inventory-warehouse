import React from 'react';
import { Platform, StatusBar } from 'react-native';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import Icon from 'react-native-vector-icons/AntDesign';

import SplashScreen from '../screens/SplashScreen';
import LoginScreen from '../screens/LoginScreen';
import BillsScreen from '../screens/BillsScreen';
import InventoryScreen from '../screens/InventoryScreen';
import SignUpScreen from '../screens/SignUpScreen';
import ProfileScreen from '../screens/ProfileScreen';
import ProductScreen from '../screens/ProductScreen';
import BillDetailScreen from '../screens/BillDetailScreen';
import InventoryDetailScreen from '../screens/InventoryDetailScreen';
import ProductDetailScreen from '../screens/ProductDetailScreen';


const headerStyle = {
    marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
}

export const SignedOut = createSwitchNavigator({
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            headerStyle
        }
    },
    SignUp: {
        screen: SignUpScreen,
        navigationOptions: {
            headerStyle
        }
    }
});

const BillDetailFlow = createSwitchNavigator({
    Bills: {
        screen: BillsScreen,
        navigationOptions: {
            headerStyle
        }
    },
    BillDetail: {
        screen: BillDetailScreen,
        navigationOptions: {
            headerStyle
        }
    }
});

const ProductDetailFlow = createSwitchNavigator({
    Products: {
        screen: ProductScreen,
        navigationOptions: {
            headerStyle
        }
    },
    ProductDetail: {
        screen: ProductDetailScreen,
        navigationOptions:{
            headerStyle
        }
    }
});

const InventoryDetailFlow = createSwitchNavigator({
    Inventories: {
        screen: InventoryScreen,
        navigationOptions: {
            headerStyle
        }
    },
    InventoryDetail: {
        screen: InventoryDetailScreen,
        navigationOptions: {
            headerStyle
        }
    }
});

export const SignedIn = createBottomTabNavigator(
    {
        Bill: {
            screen: BillDetailFlow,
            navigationOptions: {
                tabBarLabel: "Bill",
                tabBarIcon: ({ tintColor }) => (
                    <Icon name="form" size={20} color={tintColor} />
                )
            }
        },
        Bill2: {
            screen: BillDetailFlow,
            navigationOptions: {
                tabBarLabel: "Bill",
                tabBarIcon: ({ tintColor }) => (
                    <Icon name="form" size={20} color={tintColor} />
                )
            }
        },
        Product: {
            screen: ProductDetailFlow,
            navigationOptions: {
                tabBarLabel: "Product",
                tabBarIcon: ({ tintColor }) => (
                    <Icon name="car" size={20} color={tintColor} />
                )
            }
        },
        Inventory: {
            screen: InventoryDetailFlow,
            navigationOptions: {
                tabBarLabel: "Inventory",
                tabBarIcon: ({ tintColor }) => (
                    <Icon name="bank" size={20} color={tintColor} />
                )
            }
        },
        Profile: {
            screen: ProfileScreen,
            navigationOptions: {
                tabBarLabel: "Profile",
                tabBarIcon: ({ tintColor }) => (
                    <Icon name="contacts" size={20} color={tintColor} />
                )
            }
        }
    },
    {
        tabBarOptions: {
            style: {
                paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
            },
            labelStyle: {
                marginTop: 10,
                fontWeight: '500',
                marginBottom: 3
            },
            labelPosition: 'below-icon'
        },
        resetOnBlur: true
    }
);

export const createRootNavigator = () => {
    return createAppContainer(
        createSwitchNavigator(
            {
                Loading: {
                    screen: SplashScreen
                },
                SignIn: {
                    screen: SignedIn
                },
                SignOut: {
                    screen: SignedOut
                }
            },
            {
                initialRouteName: "Loading"
            }
        )
    )
}