import { AsyncStorage } from 'react-native';

const USER_AUTH = "abc";

export const onSignIn = (oauth) => {
    AsyncStorage.setItem(USER_AUTH, JSON.stringify(oauth));
};

export const getUser = async () => {
    const value = await AsyncStorage.getItem(USER_AUTH);
    if (value !== null) {
        return value;
    }
    return null;
}

export const getToken = async () => {
    await AsyncStorage.getItem(USER_AUTH)
        .then(res => {
            const value = JSON.parse(res);
            return value.access_token;
        })
        .catch(err => console.log(err))
}

export const onSignOut = async () => {
    await AsyncStorage.removeItem(USER_AUTH);
}

export const isSignedIn = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(USER_AUTH)
            .then(res => {
                if (res !== null) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
            .catch(err => reject(err));
    });
}